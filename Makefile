LATEXMKFLAGS = -lualatex -lualatex="lualatex -interaction=nonstopmode" -use-make -auxdir=aux -emulate-aux-dir

.PHONY: main.pdf all clean

# Enkel template for generelle steg, f.eks. genererte filer
# %.tex: %.raw
# 	raw2tex $< > $@


# Lag sangboka
main.pdf: main.tex titleidx.sxd titleidx.sbx
	latexmk $(LATEXMKFLAGS) main.tex

titleidx.sxd titleidx.sbx: main.tex
	latexmk $(LATEXMKFLAGS) main.tex
	texlua songidx.lua aux/titleidx.sxd aux/titleidx.sbx
	mv aux/titleidx.* .
	latexmk $(LATEXMKFLAGS) -C

clean:
	latexmk $(LATEXMKFLAGS) -C
	rm titleidx.*
